import React from 'react';
import Dashboard from '../src/Views/viewDashboard'
import Hotels from '../src/Views/viewHoteles'
import Payments from '../src/Views/viewPayments'
import Notifications from '../src/Views/viewNotifications'

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";


const App = () => {

  return (

    <Router>
      <Switch>
        <Route exact path="/">
          <Dashboard />
        </Route>
        <Route path="/hotels">
          <Hotels />
        </Route>
        <Route path="/payments">
          <Payments />
        </Route>
        <Route path="/notifications">
          <Notifications />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
