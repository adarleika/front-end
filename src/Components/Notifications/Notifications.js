import React from 'react'

import IosAnalyticsOutline from 'react-ionicons/lib/IosAnalyticsOutline'
import IosMailOutline from 'react-ionicons/lib/IosMailOutline'
import IosCard from 'react-ionicons/lib/IosCard'
import IosAppsOutline from 'react-ionicons/lib/IosAppsOutline'

import IosExitOutline from 'react-ionicons/lib/IosExitOutline'

const Notifications = () => {
    return (
        <>
            <div className="container">
                <nav className="h100vh w60 panel bcPurple flexa-jcsb flexa-ai">
                    <div className="panel">
                        {/* HOME */}
                        <a className="mt10 mb30 h50px" href="#"><IosAnalyticsOutline className="cWhite" fontSize="35px" color="#C9C9C9" beat={true} /></a>

                        {/* HOTELS   */}
                        <a className="mt30" href="#"><IosAppsOutline className="cWhite" fontSize="35px" color="#C9C9C9" /></a>

                        {/* NOTIFICATIONS */}
                        <a className="mt30" href="#"><IosMailOutline className="cWhite" fontSize="35px" color="#C9C9C9" /></a>

                        {/* PAYMENTS */}
                        <a className="mt30" href="#"><IosCard className="cWhite" fontSize="35px" color="#C9C9C9" /></a>

                    </div>
                    <a className="mb10" href="#"><IosExitOutline fontSize="35px" color="#C9C9C9" /></a>
                </nav>

                <section className="wMax1000 panel flex-one pl12em pr12em">


                <div className="sectionRight mb10">
                      <h2 className="ml10px textsize-1 fs-sbold cBlack">Notificaciones</h2>
                      <div className="notification mb30 mt10">
                          <div className="notif-left flex-display flexa-jcsb">
  
                              {/* <ion-icon className="icoGray" name="mail-open-outline"></ion-icon> */}
                              <h4 className="textsize-3">02 abril</h4>
                          </div>
                          <div className="notif-right">
                              <h4 className="mt4 textsize-2 fs-regular cBlue">Miguel Ruiz</h4>
                              <p className="mt4 textsize-3 cGray">It is a long established fact that a reader will</p>
                          </div>
                      </div>
                      <div className="notification mb30">
                          <div className="notif-left flex-display flexa-jcsb">
                              {/* <ion-icon className="icoGray" name="mail-open-outline"></ion-icon> */}
                              <h4 className="textsize-3">02 abril</h4>
                          </div>
                          <div className="notif-right">
                              <h4 className="mt4 textsize-2 fs-regular cBlue">Miguel Ruiz</h4>
                              <p className="mt4 textsize-3 cGray">It is a long established fact that a reader will</p>
                          </div>
                      </div>
                      <div className="notification mb30">
                          <div className="notif-left flex-display flexa-jcsb">
                              {/* <ion-icon className="icoGray" name="mail-open-outline"></ion-icon> */}
                              <h4 className="textsize-3">02 abril</h4>
                          </div>
                          <div className="notif-right">
                              <h4 className="mt4 textsize-2 fs-regular cBlue">Miguel Ruiz</h4>
                              <p className="mt4 textsize-3 cGray">It is a long established fact that a reader will</p>
                          </div>
                      </div>
                      <div className="notification mb30">
                          <div className="notif-left flex-display flexa-jcsb">
                              {/* <ion-icon className="icoGray" name="mail-open-outline"></ion-icon> */}
                              <h4 className="textsize-3">02 abril</h4>
                          </div>
                          <div className="notif-right">
                              <h4 className="mt4 textsize-2 fs-regular cBlue">Miguel Ruiz</h4>
                              <p className="mt4 textsize-3 cGray">It is a long established fact that a reader will</p>
                          </div>
                      </div>
                      <div className="notification mb30">
                          <div className="notif-left flex-display flexa-jcsb">
                              {/* <ion-icon className="icoGray" name="mail-open-outline"></ion-icon> */}
                              <h4 className="textsize-3">02 abril</h4>
                          </div>
                          <div className="notif-right">
                              <h4 className="mt4 textsize-2 fs-regular cBlue">Miguel Ruiz</h4>
                              <p className="mt4 textsize-3 cGray">It is a long established fact that a reader will</p>
                          </div>
                      </div>
  
                  </div>

                </section>

            </div>
        </>

    );

}

export default Notifications