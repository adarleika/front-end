//import React from 'react'
import React, { Component } from "react";
//import React, { Component, useEffect, useState } from "react";

import db from '../../api/db'


const hotels = db.hotels;
console.log(hotels)


class BuscadorHotel extends Component {

    state = {
        search: ""
    };

    renderHotel = hotel => {
        const { search } = this.state;
        let name = hotel.name.toLowerCase();

        if (search !== "" && hotel.name.toLowerCase().indexOf(search.toLowerCase()) === -1) {
            return null
        }


        return (
            <div className="col-md-3" style={{ marginTop: "60px" }}>

                <div className="row flex-dir-r w96Porc flex-wrap ">

                    <div className="card row flex-dir-c flexa-jcsb flexa-ai p10 img imghotel-1 wMin150 hMin150 m2px mt10 bcWhite">
                              <h2 className="textsize-1 fs-sbold cWhite">{hotel.name}</h2>
                              <a href="#" className="btn bcPurple">Reservar</a>
                          </div>
                </div>
            </div>
        );
    };

    onchange = e => {
        this.setState({ search: e.target.value });
    };

    render() {
        const { search } = this.state;
        const filterHotel = hotels.filter(hotel => {
            return hotel.name.toLowerCase().indexOf(search.toLowerCase()) !== -1;
        });

        return (
            <div className="flyout">
                <main style={{ marginTop: "4rem" }}>
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <input
                                    label="Buscar Hotel"
                                    icon="search"
                                    placeholder="Buscar..."
                                    onChange={this.onchange}
                                />
                            </div>
                            <div className="col" />
                        </div>
                        <div className="row">
                            {filterHotel.map(hotel => {
                                return this.renderHotel(hotel);
                            })}
                        </div>
                    </div>
                </main>
            </div>
        );
    }
}

export default BuscadorHotel

