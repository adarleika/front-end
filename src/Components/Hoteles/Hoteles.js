import React from 'react'
import IosAnalyticsOutline from 'react-ionicons/lib/IosAnalyticsOutline'
import IosMailOutline from 'react-ionicons/lib/IosMailOutline'
import IosCard from 'react-ionicons/lib/IosCard'
import IosAppsOutline from 'react-ionicons/lib/IosAppsOutline'

import IosExitOutline from 'react-ionicons/lib/IosExitOutline'

import BuscadorHotel from './Buscador'

const Hoteles = () => {
    return (
        <>
        <div className="container">
          <nav className="h100vh w60 panel bcPurple flexa-jcsb flexa-ai">
              <div className="panel">
              {/* HOME */}
                <a className="mt10 mb30 h50px" href="#"><IosAnalyticsOutline className="cWhite" fontSize="35px" color="#C9C9C9" beat={true}/></a>
                
              {/* HOTELS   */}
                <a className="mt30" href="#"><IosAppsOutline className="cWhite" fontSize="35px" color="#C9C9C9"/></a>
  
              {/* NOTIFICATIONS */}
                <a className="mt30" href="#"><IosMailOutline className="cWhite" fontSize="35px" color="#C9C9C9"/></a>
              
              {/* PAYMENTS */}
                <a className="mt30" href="#"><IosCard className="cWhite" fontSize="35px" color="#C9C9C9"/></a>
              
              </div>          
              <a className="mb10" href="#"><IosExitOutline fontSize="35px" color="#C9C9C9"/></a>
          </nav>
          
          <section className="wMax1000 panel flex-one pl12em pr12em">
  
  
              <div className="containerCenter">
                  <div className="row flex-dir-c">
                    
                      <h2 className="ml10px mt10 textsize-1 fs-sbold cBlack">Hoteles Disponibles</h2>
                      <BuscadorHotel />
                      <div className="row flex-dir-r w96Porc flex-wrap ">
  
    
                      </div>
  
                  </div>
                  
  
              </div>
  
          </section>
          
        </div>     
      </>

        );

    }
    
    export default Hoteles